import os, shutil
originalDataSetPathDir = './dataset/dogs-vs-cats/train'
baseDir = './result'
os.mkdir(baseDir)
trainDir = os.path.join(baseDir, 'train')
os.mkdir(trainDir)

validationDirectory = os.path.join(baseDir,'validation')
os.mkdir(validationDirectory)

testDir = os.path.join(baseDir,'test')
os.mkdir(testDir)

trainCatDir = os.path.join(trainDir,'cats')
os.mkdir(trainCatDir)

trainDogDir = os.path.join(trainDir,'dogs')
os.mkdir(trainDogDir)

validationCatDir = os.path.join(validationDirectory,'cats')
os.mkdir(validationCatDir)

validationDogDir = os.path.join(validationDirectory,'dogs')
os.mkdir(validationDogDir)

testCatDir = os.path.join(testDir, "cats")
os.mkdir(testCatDir)

testDogDir = os.path.join(testDir, "dogs")
os.mkdir(testDogDir)

fnames = ['cat.{}.jpg'.format(i) for i in range(1000)]
for fname in fnames:
    src = os.path.join(originalDataSetPathDir,fname)
    dst = os.path.join(trainCatDir,fname)
    shutil.copy(src, dst)

fnames = ['cat.{}.jpg'.format(i) for i in range(1000,1500)]
for fname in fnames:
    src = os.path.join(originalDataSetPathDir, fname)
    dst = os.path.join(validationCatDir, fname)
    shutil.copy(src, dst)


fnames = ['cat.{}.jpg'.format(i) for i in range(1500, 2000)]
for fname in fnames:
    src = os.path.join(originalDataSetPathDir, fname)
    dst = os.path.join(testCatDir, fname)
    shutil.copyfile(src, dst)

fnames = ['dog.{}.jpg'.format(i) for i in range(1000)]
for fname in fnames:
    src = os.path.join(originalDataSetPathDir,fname)
    dst = os.path.join(trainDogDir,fname)
    shutil.copy(src, dst)

fnames = ['dog.{}.jpg'.format(i) for i in range(1000,1500)]
for fname in fnames:
    src = os.path.join(originalDataSetPathDir, fname)
    dst = os.path.join(validationDogDir, fname)
    shutil.copy(src, dst)


fnames = ['dog.{}.jpg'.format(i) for i in range(1500, 2000)]
for fname in fnames:
    src = os.path.join(originalDataSetPathDir, fname)
    dst = os.path.join(testDogDir, fname)
    shutil.copyfile(src, dst)

print('total training cat images:', len(os.listdir(trainCatDir)))
print('total training dog images:', len(os.listdir(trainDogDir)))
print('total validation cat images:', len(os.listdir(validationCatDir)))
print('total validation dog images:', len(os.listdir(validationDogDir)))
print('total test cat images:', len(os.listdir(testCatDir)))
print('total test dog images:', len(os.listdir(testDogDir)))