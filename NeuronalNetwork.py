from functionBackward import *
import numpy as np


class NeuronalNetwork(object):
    def __init__(self, layer):
        assert (isinstance(layer, list))
        self.layer = layer
        self.bias = []
        self.weight = []
        self.eLuHyperParameter = 1
        for i in range(1, len(layer)):
            self.bias.append(np.zeros((layer[i],1)))
            self.weight.append(np.random.randn(layer[i], layer[i-1]))

        print("W : " + str(self.weight) + " b : " + str(self.bias))


    def linearForward(self, A, weightValue, bValue):
        print("premiere étape : " + str(weightValue) + "  " + str(bValue))
        print("W shape : " + str(self.weight[weightValue].shape) + "A shape : " + str(A.shape))
        Z = self.weight[weightValue] @ A + self.bias[bValue]
        cache = (A, self.weight[weightValue], self.bias[bValue])
        return Z, cache

    def linearActivationForward(self, previousA, W, b, activation):
        print("W " + str(W) + " b " + str(b))
        if activation == "sigmoid":
            Z, linear_cache = self.linearForward(previousA, W, b)
            A, activation_cache = sigmoid(Z)

        elif activation == "relu":
            Z, linear_cache = self.linearForward(previousA, W, b)
            A, activation_cache = relu(Z)

        elif activation == "leaky relu":
            Z, linear_cache = self.linearForward(previousA, W, b)
            A, activation_cache = leakyRelu(Z)

        return A, activation_cache

    def cost(self, AL, Y):
        m = Y.shape[1]
        cost = -(1 / m) * np.sum(np.log(AL) @ Y)
        cost = np.squeeze(cost)
        return cost

    def linearBackward(self, dZ, cache, previousA):
        m = previousA.shape[1]
        dW = dZ @ cache[0].T / m
        db = np.squeeze(np.sum(dZ, axis=1, keepdims=True)) / m
        dA_prev = cache[1].T @ dZ
        return dA_prev, dW, db

    def linearActivationBackward(self, dA, cache, activation):
        linear_cache, activation_cache = cache
        if activation == "sigmoid":
            dz = sigmoid_backward(dA, activation_cache)
        elif activation == "relu":
            dz = relu_backward(dA, activation_cache)
        elif activation == "leaky relu":
            dz = leakyReluBackward(dA, activation_cache)
        elif activation == "elu":
            dz = eluBackward(dA, activation_cache, self.eLuHyperParameter)
        return self.linearBackward(dz, linear_cache)

    def LmodelForward(self, X):
        print("longueur totale : " + str(len(self.layer)-1))
        caches = []
        A = X
        for l in range(0, len(self.layer)-2):
            A_prev = A
            A, cache = self.linearActivationForward(A_prev, l, l, activation='relu')
            caches.append(cache)

        AL, cache = self.linearActivationForward(A,
                                                 len(self.layer)-2,
                                                 len(self.layer)-2,
                                                 activation='sigmoid')
        caches.append(cache)
        return AL, caches